#!/usr/bin/python
import shelve
from urllib import parse
import webapp
import random
import string

contents = shelve.open('contents')

FORM = """
<!DOCTYPE html>
<html lang="eng">
    <body>
        <div>
            <h2> WELCOME TO THE PAGE_FORM </h2>
        </div>
        <div>
            <form action="/" method="post">
                <div>
                    <label> URL2SHORT: </label>
                    <input type="text" name="url" required>
                    <input type="submit" value="Submit"
                </div>
            </form>
        </div>
        <div>
            <p> URL LIST: <br>
                {urllist}
            </p>
        </div>
    </body>
</html>
"""
PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="eng">
    <body>
        <h1> 
            Method not allowed: {method}.
        </h1>
    </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""


PAGE_POST = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <div>
            <p>Shorten url: {body}.</p>
        </div>
        <div>
            <form action="/" method="post">
                <div>
                    <label> URL2SHORT: </label>
                    <input type="text" name="url" required>
                    <input type="submit" value="Submit"
                </div>
            </form>
        </div>
        
        <div>
            <p>URL LIST:<br> {urllist}</p>
        </div>
    </body>
</html>
"""


class RandomShort(webapp.webApp):
    """Simple web application for shortening urls"""

    def parse (self, request):
        """Return the resource name (/ removed)"""

        dic = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            dic['body'] = None
        else:
            dic['body'] = request[body_start + 4:]

        dic['method'] = request.split(' ')[0]
        dic['resource'] = request.split(' ')[1]

        return dic

    def process(self, dic):
        if dic['method'] == 'GET':
            code, page = self.get(dic['resource'])
        elif dic['method'] == 'POST':
            code, page = self.post(dic['resource'], dic['body'])
        else:
            code, page = "404 Method Not Allowed" , \
                        PAGE_NOT_ALLOWED.format(method=dic['method'])

        return code, page

    def get(self, resource):
        if resource == "/":
            code = "200 OK"
            page = FORM.format(urllist=self.fulldic(contents))

        elif resource == "/favicon.ico":
            code = "204 No Content"
            page = ""

        elif resource in contents:   #resource aqui en mi clave que es la url acortada
            url = contents[resource]
            page = ""
            code = "301 Moved Permanently\r\n New Location: {location}\r\n\r\n".format(location=url)

        return code, page

    def post(self, resource, body):
        args = parse.parse_qs(body) #me devuelve un diccionario con los campos como clave y el contenido su valor
        if resource == "/": #esto quiere decir que la solicitud se ha enviado desde el formulario, es decir, la pagina principal que es la /
            if "url" in args:
                url = self.fullurl(args['url'][0]) #como el valor del campo URL y aplico la funcion fullurl para ver si esta bien escrita
                if url in contents.values(): #me devuelve una lista con todos los valores(url reales) y veo si coincide alguna con mi url
                    shorten_url = list(contents.keys())[list(contents.values()).index(url)] #aqui lo que hacemos es como nos han introducido una url que ya se habia acortado antes, obtenemos de contents su url acortada (en este caso la clave asignada al valor q es la url real introducida)
                    print(shorten_url)
                    page = PAGE_POST.format(body=shorten_url, urllist=self.fulldic(contents))
                    code = "200 Ok"
                else:
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    shorten_url = '/' + random_key
                    contents[shorten_url] = url #aqui lo que hago es asignar el valor de mi url real a la clave creada
                    print(shorten_url)
                    page = PAGE_POST.format(body=shorten_url, urllist=self.fulldic(contents))
                    code = "200 Ok"
            else:
                page = PAGE_UNPROCESSABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            page = PAGE_UNPROCESSABLE.format(body=body)
            code = "422 Unprocessable Entity"

        return code, page

    def fulldic(self, content):
        dic = ""
        for clave in content:
            dic = dic + "Short URL: " + clave + " - " + "Real URL: " + content[clave] + "<br>"
        return dic

    def fullurl(self, url):
        if "http://" in url or "https://" in url:
            return url
        else:
            return "https://" + url






if __name__ == "__main__":
    webapp = RandomShort("localhost", 1234)